From node:12
# specify a suitable source image

WORKDIR /app

COPY package*.json ./

RUN npm ci

COPY . .
# copy the application source code files

EXPOSE 3000

CMD ["npm", "start"]
# specify the command which runs the application